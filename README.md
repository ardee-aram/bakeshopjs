# [BakeshopJS](https://bitbucket.org/ardee-aram/bakeshopjs) 

BakeshopJS is an online storefront written in AngularJS, and uses GCore as retail backend. 
The goals of BakeshopJS are the following:

- To create a store front that can be used to showcase and sell online cakes and pastries.
- To serve as completion metric for the Gcore API ("The Gcore API Store Front is complete when one can build a logic-less layer on top of Gcore").
- To serve as a feasibility case for a purely AngularJS store front.  

This uses [ng-boilerplate](https://github.com/ngbp/ngbp) as its AngularJS framework.

***

## Quick Start

Install Node.js and then:

```sh
$ git clone git@bitbucket.org:ardee-aram/bakeshopjs.git
$ cd bakeshopjs
$ sudo npm -g install grunt-cli karma bower
$ npm install
$ bower install
$ grunt watch
```

Configure your web server to point to `/path/to/bakeshopjs/build`.

Happy hacking!